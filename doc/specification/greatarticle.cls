\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{greatarticle}

\LoadClass[a4paper, 11pt]{report}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{graphicx}
\RequirePackage{titlesec}
\RequirePackage{parskip}

\titleformat{\chapter}[hang]
{\Huge\bfseries}
{\thechapter\hspace{20pt}{|}\hspace{20pt}}
{0pt}
{\huge\bfseries}

\def\@university{}
\def\@majorheading{}
\def\@minorheading{}

\newcommand{\university}[1]{
    \def\@university{#1}
}
\newcommand{\majorheading}[1]{
    \def\@majorheading{#1}
}
\newcommand{\minorheading}[1]{
    \def\@minorheading{#1}
}

\newif\ifsupervisor
\newif\iflogo
\supervisorfalse
\logofalse

\newcommand{\supervisor}[2][Supersvisor]{
    \supervisortrue
    \def\@supervisorheader{#1}
    \def\@supervisor{#2}
}
\newcommand{\logo}[1]{
    \logotrue
    \def\@logo{#1}
}

\renewcommand\author[2][Author]{
    \def\@authorheader{#1}
    \def\@author{#2}
}

\renewcommand\maketitle{

    \newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

    \begin{titlepage}

        \center

        \iflogo
        \includegraphics{\@logo}\\[1cm]
        \fi

        \textsc{\LARGE \@university}\\[1.5cm]
        \textsc{\Large \@majorheading}\\[0.5cm]
        \textsc{\Large \@minorheading}\\[0.5cm]

        \HRule \\[0.4cm]
        { \huge \bfseries \@title}\\[0.4cm]
        \HRule \\[1.5cm]

        \ifsupervisor
        \begin{minipage}{0.4\textwidth}
            \begin{flushleft} \large
                \emph{\@authorheader:}\\
                \@author
            \end{flushleft}
        \end{minipage}
        ~
        \begin{minipage}{0.4\textwidth}
            \begin{flushright} \large
                \emph{\@supervisorheader:}\\
                \@supervisor
            \end{flushright}
        \end{minipage}\\[4cm]
        \else
        \Large \emph{Author:}\\
        \@author\\[4cm]
        \fi

        {\large \@date}\\[3cm]

        \vfill

    \end{titlepage}
}
