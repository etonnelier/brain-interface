# LibSVM classification algorithm

********************************

# linear kernel

## data : eegSmooth_140.mat
## users classification
cross-validation (7-fold) :
maxSuccess : 0.81675 std : 0.011
maxSuccessClass1 : 0.71199 std : 0.016
maxSuccessClass2 : 0.97288 std : 0.007
maxSuccessClass3 : 0.84642 std : 0.022

## letters classification
cross-validation (7-fold) :
maxSuccess : 0.053482 std : 0.038513
maxSuccessClass1 : 0.028434 std : 0.021899
maxSuccessClass2 : 0.040143 std : 0.0083603
maxSuccessClass3 : 0.047860 std : 0.028517
maxSuccessClass4 : 0.061059 std : 0.043953
maxSuccessClass5 : 0.078228 std : 0.085536
maxSuccessClass6 : 0.076558 std : 0.041674
maxSuccessClass7 : 0.065191 std : 0.026947
maxSuccessClass8 : 0.059186 std : 0.058735
maxSuccessClass9 : 0.048461 std : 0.057562
maxSuccessClass10 : 0.063685 std : 0.038667
maxSuccessClass11 : 0.036155 std : 0.033530
maxSuccessClass12 : 0.056230 std : 0.024377
maxSuccessClass13 : 0.060772 std : 0.030375
maxSuccessClass14 : 0.059519 std : 0.032002
maxSuccessClass15 : 0.050569 std : 0.032618
maxSuccessClass16 : 0.042016 std : 0.042904
maxSuccessClass17 : 0.076933 std : 0.079387
maxSuccessClass18 : 0.048247 std : 0.031026
maxSuccessClass19 : 0.058797 std : 0.036661
maxSuccessClass20 : 0.060698 std : 0.039660
maxSuccessClass21 : 0.015712 std : 0.019630
maxSuccessClass22 : 0.037259 std : 0.029194
maxSuccessClass23 : 0.063201 std : 0.022869
maxSuccessClass24 : 0.054142 std : 0.051577
maxSuccessClass25 : 0.049305 std : 0.030352
maxSuccessClass26 : 0.052168 std : 0.053317
