## Author: Emeric Tonnelier
## Author: Thomas Da Costa

## -*- texinfo -*- ##
## @deftypefn {Function File} @var{eeg} = parseMarseilleData(@var{data})
## Return an eegObject from Marseille data matrix
## [nbUsers x nbLetters x nbSensors x nbTimes x nbExperiences].
## @end deftypefn

function eeg = parseMarseilleData(data)
  if(nargin != 1)
    print_usage();
  endif
  nbUsers = size(data, 1);
  nbLetters = size(data, 2);
  nbSensors = size(data, 3);
  nbTimes = size(data, 4);
  nbExperiences = size(data, 5);
  x = [];
  y = [];
  for user = 1 : nbUsers
    for letter = 1 : nbLetters
      c = 0;
      for experience = 1 : nbExperiences
      	if(isnan(data(user, letter, 1, 1, experience)))
      	  break;
      	else
          c++;
      	endif
      endfor
      d = data(user, letter, :, :, 1:c);
      d = permute(d, [1, 2, 5, 4, 3]);
      d = reshape(d, c, nbSensors * nbTimes);
      x = [x; d];
      y = [y; repmat([user, letter], c, 1)];
    endfor
  endfor
  eeg = eegObject(nbSensors, nbTimes, x, y);
endfunction
