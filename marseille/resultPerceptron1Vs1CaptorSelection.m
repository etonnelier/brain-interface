## Author: Emeric Tonnelier
## Author: Thomas Da Costa

pkg load eeg;

addpath ../data

clear all;
close all;

files = {...
  "bestCaptorsSubset20Ind.mat" ...
  "bestCaptorsSubset50Ind.mat", ...
};

name = sprintf("%s.md", mfilename);
fid = fopen(name, "w");

if(fid == -1)
  error("unable to open %s", name);
end

fprintf(fid, "# perceptron 1VS1 captor selection learning algorithm\n\n")
fprintf(fid, "**************************\n\n");
fprintf(fid, "## script\n");
fprintf(fid, "%s.m\n", mfilename);
load("eegSmooth_075.mat");
fprintf("### users classification\n");
fprintf("### Data tested : eegSmooth_075.mat"); 
for i = 1 : numel(files)
  file = files{i};
  load(file);
  fprintf(fid, "\n## %s\n", file);
  printf("File %d/%d Optimize Perceptron 1VS1 captor selection\n", i, numel(files));
  fflush(stdout);
  [p, oe, oit] = optimizePerceptron1Vs1CaptorSelection(eeg, 1, 1e-3, [7000:1000:10000], bestSubset, size(bestSubset, 2));
  fprintf(fid, "maxSuccess : %g epsilon : %g nIter : %g\n", p, oe, oit);
  fflush(fid);
end

fclose(fid);

rmpath ../data
