# perceptron 1VS1 learning algorithm
# Captors selection results

**************************

## donnees : eegSmooth_140.mat

## bestCaptorsSubset20Ind.mat
optimizePerceptron : maxSuccess : 0.65236 epsilon : 0.001 nIter : 8000

## bestCaptorsSubset40Ind.mat
optimizePerceptron : maxSuccess : 0.67310 epsylon : 0.001 nIter : 8000

## bestCaptorsSubset50Ind.mat
optimizePerceptron : maxSuccess : 0.72416 epsilon : 0.01 nIter : 10000

## bestCaptorsSubset60Ind.mat
optimizePerceptron : maxSuccess : 0.71876 epsilon : 0.01 nIter : 9000
