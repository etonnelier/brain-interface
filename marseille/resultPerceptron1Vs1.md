# perceptron 1VS1 learning algorithm

**************************

## script
resultPerceptron1Vs1.m

## eegSmooth_140.mat
### users classification
cross-validation (10-fold): maxSuccess : 0.721035 epsilon : 0.001 nIter : 9000
### letters classification
cross-validation (10-fold): maxSuccess : 0.126822 epsilon : 0.3 nIter : 7000 
