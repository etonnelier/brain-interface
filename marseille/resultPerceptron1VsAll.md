# perceptron learning algorithm

**************************

## script
resultPerceptron1VsAll.m

## eegSmooth_075.mat
### users classification
cross-validation (10-fold): maxSuccess : 0.857381 epsilon : 0.001
### letters classification
cross-validation (10-fold): maxSuccess : 0.0398094 epsilon : 0.001

## eegSmooth_140.mat
### users classification
cross-validation (10-fold): maxSuccess : 0.68946 epsilon : 0.1
### letters classification
cross-validation (10-fold): maxSuccess : 0.0412324 epsilon : 0.001

## eegSmooth_125.mat
### users classification
cross-validation (10-fold): maxSuccess : 0.815155 epsilon : 0.0001
### letters classification
cross-validation (10-fold): maxSuccess : 0.0399546 epsilon : 0.01

## eegSmooth_100.mat
### users classification
cross-validation (10-fold): maxSuccess : 0.856373 epsilon : 0.01
### letters classification
cross-validation (10-fold): maxSuccess : 0.0395224 epsilon : 0.01
