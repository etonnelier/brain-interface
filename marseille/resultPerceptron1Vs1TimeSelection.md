# perceptron 1VS1 learning algorithm
# time selection algorithme

*********************************

# donnees : eegSmooth_140.mat

## bestTimeSubset10Ind.mat

## Without time selection (27 sensors):
maxSuccess : 0.77335 epsilon : 0.001 nIter : 76000

## 25 sensors :
maxSuccess : 0.74946 epsilon : 0.4 nIter : 31000

## 20 sensors :
maxSuccess : 0.76068 epsilon : 0.3 nIter : 34000

## 15 sensors :
maxSuccess : 0.75954 epsilon : 0.6 nIter : 34000

## 10 sensors :
maxSuccess : 0.76327 epsilon : 0.3 nIter : 34000
