## Author: Emeric Tonnelier
## Author: Thomas Da Costa

## This script is used in order to extract and filter data.

addpath ..
addpath ../filter

fprintf("loading data...");
fflush(stdout);
load donnees_erp_lettres.mat
fprintf(" done\n");
fflush(stdout);

fprintf("extracting data... ");
fflush(stdout);
eegStandard = parseMarseilleData(donnees_erp_lettres(1:3, :, :, :, :));
fprintf("done\n");
fflush(stdout);

fprintf("saving eegStandard.mat... ");
fflush(stdout);
save("-binary", "eegStandard.mat", "eegStandard");
fprintf("done\n");
fflush(stdout);

smooth = [75, 100, 125, 140];

for i = 1 : length(smooth)
  s = smooth(i);
  name = sprintf("eegSmooth_%03d.mat", s);
  fprintf("%s process:\n", name);
  fflush(stdout);
  eeg = lowpass(eegStandard, normpdf([-s : s]));
  fprintf("saving %s... ", name);
  fflush(stdout);
  save("-binary", name, "eeg");
  fprintf("done\n");
  fflush(stdout);
end
