## Author: Emeric Tonnelier
## Author: Thomas Da Costa

addpath ..
addpath ../learning

files = {...
  "eegSmooth_140.mat" ...
  "eegSmooth_125.mat", ...
  %"eegSmooth_100.mat", ...
%"eegSmooth_075.mat", ...
};

name = sprintf("%s.md", mfilename);
fid = fopen(name, "w");

if(fid == -1)
  error("unable to open %s", name);
end

fprintf(fid, "# basic learning algorithm\n\n")
fprintf(fid, "**************************\n\n");
fprintf(fid, "## script\n");
fprintf(fid, "%s.m\n", mfilename);

for i = 1 : numel(files)
  file = files{i};
  load(file);
  fprintf(fid, "\n## %s\n", file);
  fflush(fid);
  for l = 1 : 2
    if(l == 1)
      fprintf(fid, "### users classification\n");
    else
      fprintf(fid, "### letters classification\n");
    end
    for p = 10
      fprintf(fid, "cross-validation (%d-fold): ", p);
      fflush(fid);
      succes = crossValidation(eeg, l, p, @learnBasic, @predictBasic);
      fprintf(fid, "mean = %g, std = %g\n", mean(succes), std(succes));
    end
  end
end

fclose(fid);
