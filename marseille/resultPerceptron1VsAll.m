## Author: Emeric Tonnelier
## Author: Thomas Da Costa

addpath ..
addpath ../learning
addpath ../data

clear all;
close all;

files = {...
  "eegSmooth_075.mat", ...
  "eegSmooth_140.mat" ...
  "eegSmooth_125.mat", ...
  "eegSmooth_100.mat", ...
};

name = sprintf("%s.md", mfilename);
fid = fopen(name, "w");

if(fid == -1)
  error("unable to open %s", name);
end

fprintf(fid, "# perceptron learning algorithm\n\n")
fprintf(fid, "**************************\n\n");
fprintf(fid, "## script\n");
fprintf(fid, "%s.m\n", mfilename);

for i = 1 : numel(files)
  file = files{i};
  load(file);
  fprintf(fid, "\n## %s\n", file);
  fflush(fid);
  for l = 1 : 2
    if(l == 1)
      fprintf(fid, "### users classification\n");
    else
      fprintf(fid, "### letters classification\n");
    end
    for p = 10
      printf("File %d/%d test %d/2 cross-validation\n", i, numel(files), l);
      fflush(stdout);
      fprintf(fid, "cross-validation (%d-fold): ", p);
      fflush(fid);
      %succes = crossValidation(eeg, l, p, @learnPerceptron, 0.3, 1000, @predictPerceptron);
      [p, oe, oit] = optimizePerceptron(eeg, l, 1e-4, [1000:1000:10000]);
      fprintf(fid, "maxSuccess : %g epsilon : %g\n", p, oe);
      fflush(fid);
    end
  end
end

fclose(fid);

rmpath ..
rmpath ../learning
rmpath ../data