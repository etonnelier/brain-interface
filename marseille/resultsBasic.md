# basic learning algorithm

**************************

## script
resultsBasic.m

## eegSmooth_140.mat
### users classification
cross-validation (10-fold): mean = 0.76667, std = 0.0168233
### letters classification
cross-validation (10-fold): mean = 0.0452129, std = 0.0069741

## eegSmooth_125.mat
### users classification
cross-validation (10-fold): mean = 0.732539, std = 0.0217984
### letters classification
cross-validation (10-fold): mean = 0.0418032, std = 0.00403765
