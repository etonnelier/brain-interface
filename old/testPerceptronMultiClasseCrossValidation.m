addpath("./data", "./classification", "./refactoring", "./visualisation");
clear all;
close all;

load("donneesSelectMoy.mat");


dimensionTest = 1;

X = X([1:end-33],:);
Y = Y([1:end-33], dimensionTest);

[X, Y] = shakeExperiences(X, Y);

[perfMoy, perfEcartType, perf, perfMoyenneParClasse, perfEcartTypeParClasse,model] = crossValidation_PMC(X, Y, 7, 0.3, 3000);

figure;
bar(perfMoyenneParClasse);
title("Performances moyennes par classe et experiences - Perceptron multi-classes");
xlabel("experiences");
ylabel("performance");

rmpath("./data", "./classification", "./refactoring", "./visualisation");