addpath("./data", "./classification", "./refactoring", "./visualisation");
clear all;
close all;

load("donnees_test_split_moy.mat");


dimensionTest = 1;

X = [xapp; xtest([1:end-33],:)];
Y = [yapp(:,dimensionTest); ytest([1:end-33], dimensionTest)];

[X, Y] = shakeExperiences(X, Y);

[perfMoy, perfEcartType, perf, perfMoyenneParClasse, perfEcartTypeParClasse,model] = crossValidation_P1C1(X, Y, 7, 0.3, 3000);

figure;
bar(perfMoyenneParClasse);
title("Performances moyennes par classe et experiences - Perceptron 1 contre 1");
xlabel("experiences");
ylabel("performance");

rmpath("./data", "./classification", "./refactoring", "./visualisation");