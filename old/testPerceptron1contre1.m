addpath("./classification");
addpath("./data");
addpath("./refactoring");

clear all;
close all;
nbTest = 4000;
dimensionTest = 1;
load("donnees_test_split_moy.mat");
myOnes1 = ones(size(xapp, 1), 1);
myOnes2 = ones(size(xtest, 1), 1);
xapp = [xapp, myOnes1];
xtest = [xtest, myOnes2];
[w, combinaisons] = learn_P1C1(xapp, yapp(:,dimensionTest), 0.3, 3000);
yPred = estimation_P1C1(xtest, w, combinaisons);
success = mean(ytest(:,dimensionTest) == yPred);

rmpath("./classification", "./data", "./refactoring");