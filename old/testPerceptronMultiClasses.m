addpath("./classification");
addpath("./data");
addpath("./refactoring");

clear all;
close all;
nbTest = 4000;
dimensionTest = 1;
%load("usps.dat");
%load("donnees_erp_lettres.mat" );
%[X,Y] = split_eeg_to_XY(donnees_erp_lettres);
%load("donnees_split_eeg_to_XY_06021014.mat");
load("donnees_test_split_moy.mat");
%w = learn_PMC(X, Y(:,dimensionTest), 0.4, 500);
myOnes1 = ones(size(xapp, 1), 1);
myOnes2 = ones(size(xtest, 1), 1);
xapp = [xapp, myOnes1];
xtest = [xtest, myOnes2];
w = learn_PMC(xapp, yapp(:,dimensionTest), 0.3, 1200);
%w = learn_PMC_Stocrastique(xapp, yapp, 0.3, 2000, 0.00001);
%load("w_learn_PMC_05022014.mat");
%testIndices = abs(round(rand(nbTest, 1)*size(X, 1)));
%myX = X(testIndices,:);
%myY = Y(testIndices,dimensionTest);
%yPred = estimation_PMC(myX, w);
yPred = estimation_PMC(xtest, w);
success = mean(ytest(:,dimensionTest) == yPred);

rmpath("./classification", "./data", "./refactoring");