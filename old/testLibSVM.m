addpath("./classification/libsvm-3.17/matlab");
addpath("./data");

clear all;
close all;
nbTest = 4000;
dimensionTest = 1;
load("donnees_test_split_moy.mat");

w = svmtrain(yapp(:,dimensionTest), xapp, "-s 1 -t 0");

[predicted_label] = svmpredict(ytest(:,dimensionTest), xtest, w);

rmpath("./classification/libsvm-3.17/matlab", "./data");