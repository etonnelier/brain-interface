function [historyCaptors, historyTemps, history] = historiquePonderationPMC(X, w, nbCapteur, nbTemps)

%     calculate statistics about the estimation processus over data X and w model
%     return historiy by captors and time
%
%     X : [experience; caracteristics]
%
%     w : [model; weights]
%
%     nbCapteur : number of captors in each experiences ! nbCapteur * nbTemps must be equal to size(X, 2)
%
%     nbTemps : number of time units in each experiences ! nbCapteur * nbTemps must be equal to size(X, 2)
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  historyTemps = zeros(size(X, 1), nbTemps);
  historyCaptors = zeros(size(X, 1), nbCapteur);
  history = zeros(size(X, 1), size(X, 2));
  
  for class = 1 : size(w, 2)
  printf("constResult : Classe : %d/%d\n", class, size(w, 2));
  fflush(stdout);
    for xp = 1 : size(X, 1)
      if (mod(xp, 100) == 0)
        printf("constResult : xp : %d/%d\n", xp, size(X, 1));
        fflush(stdout);
      endif
      for caract = 1 : size(X, 2)
        history(xp, caract) = X(xp, caract) * w(caract, class);
      endfor
    endfor
  endfor
  for xp = 1 : size(history, 1)
    printf("ConstTemps : xp : %d/%d\n", xp, size(history, 1));
    fflush(stdout);
    for temps = 1 : nbTemps
      for caractQ = 1 : size(history, 2);
        if mod(caractQ, nbTemps) == temps
          historyTemps(xp, temps) += abs(history(xp, caractQ));
        endif
      endfor
    endfor
  endfor
  for xp = 1 : size(history, 1)
    printf("ConstCapt : xp : %d/%d\n", xp, size(history, 1));
    fflush(stdout);
    for capt = 1 : nbCapteur
      for caractQ = 1 : size(history, 2)
        if (mod(caractQ, nbCapteur) == capt)
          historyCaptors(xp, capt) += abs(history(xp, caractQ));
        endif
      endfor
    endfor
  endfor
endfunction