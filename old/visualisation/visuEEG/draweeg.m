clear all
close all

coord = load('coord.txt');
% deg -> rad
coord = coord / 180 * pi;

## size(coord)

## x = cos(coord(:,1)) .* cos(coord(:,2));
## y = cos(coord(:,1)) .* sin(coord(:,2));
## z = sin(coord(:,1));

## figure
## plot3(x,y,z, 'o')
## hold on
## for i=1:size(x,1)
##   text(x(i)+0.02,y(i),z(i), sprintf('%d',i));
## end
## view(2)

% projection autour de (0,0,0)
x2 = cos(coord(:,2)) .* coord(:,1);
y2 = sin(coord(:,2)) .* coord(:,1);


function proj(eeg, coord, sigma)

	 xgrid = generateGrid(coord, 30);
	 k = svmkernel(xgrid, 'gaussian', sigma, coord);
	 ygrid = k * eeg;
	 plotFrontiere(xgrid, ygrid, 1);
	 shading('interp');

endfunction


sigma = 0.3;
eeg = rand(64,1)*0.1;
eeg(10) = 1;

figure
proj(eeg, [x2,y2], sigma);
view(2)
grid off
%plot3(x2,y2,ones(size(x2,1),1),'ro');

hold on
plot(x2,y2,'ro');
for i=1:size(coord,1)
  %text(x2(i)+0.02,y2(i), 1, sprintf('%d',i));
  text(x2(i)+0.02,y2(i), sprintf('%d',i));
end
%view(2)
