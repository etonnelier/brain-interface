addpath("./classification");
addpath("./visualisation");
addpath("./data");
addpath("./refactoring");

clear all;
close all;
nbTest = 4000;
dimensionTest = 1;
load("donnees_XY.mat");
X = reshape(X, size(X,1), size(X,2)*size(X,3));
myOnes = ones(size(X ,1), 1);
X = [X, myOnes];
[X, Y] = shakeExperiences(X, Y);
xapp = X([1:1000], :);
yapp = Y([1:1000], dimensionTest);
xtest = X([1001:end], :);
ytest = Y([1001:end], dimensionTest);
w = learn_PMC(xapp, yapp(:,dimensionTest), 0.3, 5000);
yPred = estimation_PMC(xtest(1,:), w);
[historyCaptors, historyTemps, history] = historiquePonderationPMC(xtest(1,:), w, 64, 307);

figure;
plot(historyCaptors(1,:));
title('Historique de pondération des caractéristiques par capteurs');
xlabel('Capteurs');
ylabel('pondération');

figure;
plot(historyTemps(1,:));
title('Historique de pondération des caractéristiques par unité de temps');
xlabel('temps');
ylabel('pondération');

rmpath("./classification", "./data", "./refactoring", "./visualisation");