function [w] = learn_PMC_Stocrastique(xapp, yapp, epsilon, nIter, lambda)

%     learn data X caracteristics acording to y labels
%     Perceptron model and stocrastic strategy
%     make it iterations throught data.
%     return a w model
%
%     xapp : [experience; caracteristics]
%
%     yapp : [model; weights]
%
%     epsilon : number +
%
%     nIter : number +
%
%     lambda : number +
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  nbClass = size(unique(yapp), 1);
  w = randn(size(xapp, 2), nbClass);  
  s = size(xapp, 1) - 1;
  size(w) 
  for i = 1 : nIter
    index = round(s * rand() + 1);
    xi = xapp(index, :);
    for class = 1 : nbClass    
      if (yapp(index) == class)
        yi=1;
      else
        yi=-1;
      endif
      if (yi * xi * w(:, class)) < 1
        w(:, class) += epsilon * xi' * yi;
      endif
      w(:, class) += (-2 * lambda * epsilon * w(:, class));
    endfor
  endfor
endfunction