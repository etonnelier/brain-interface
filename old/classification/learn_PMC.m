function [w] = learn_PMC(xapp, yapp, epsilon, nIter)

%     learn data X caracteristics acording to y labels
%     Perceptron model
%     make it iterations throught data.
%     return a w model
%
%     xapp : [experience; caracteristics]
%
%     yapp : [model; weights]
%
%     epsilon : number +
%
%     nIter : number +
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  nbClass = size(unique(yapp), 1);
  w = randn(size(xapp, 2), nbClass);  
  s = size(xapp, 1) - 1;
  for i = 1 : nIter
    index = round(s * rand() + 1);
    xi = xapp(index, :);
    for class = 1 : nbClass    
      if (yapp(index) == class)
        yi=1;
      else
        yi=-1;
      endif
      if (yi * xi * w(:, class)) <= 0
        w(:, class) = w(:, class) + epsilon * xi' * yi;
      endif
    endfor
  endfor
endfunction