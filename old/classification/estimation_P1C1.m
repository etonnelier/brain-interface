function [yPred] = estimation_P1C1(X, w, combinaisons)

%     estimate class from data X with a w multi-class perceptron model
%     return estimate classes
%
%     X : [experience; caracteristics]
%
%     w : [model; weights]
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  nbClass = size(w, 1);
  results = X * w; 
 [maxValue, maxInd] = max(results,[],2);
 yPred = combinaisons(maxInd, 1);
endfunction