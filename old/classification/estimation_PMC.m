function [yPred] = estimation_PMC(X, w)

%     estimate class from data X with a w multi-class perceptron model
%     return estimate classes
%
%     X : [experience; caracteristics]
%
%     w : [model; weights]
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  nbClass = size(w, 1);
  results = X * w; 
 [maxValue, yPred] = max(results,[],2);
endfunction