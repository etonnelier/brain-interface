function [perfMoyenne, perfEcartType, perfs, perfMoyenneParClasse, perfEcartTypeParClasse, w] = crossValidation_PMC(X, Y, nbTest, epsylon, nbIterApp)

%     perform an cross validation on the X data and Y labels.
%     Perceptron model
%     return statistics about performances
%
%     X : [experiences; caracteristics]
%
%     Y : [experiences; labels]
%
%     nbTest : number : number of test for validation
%
%     epsilon : number + (learning parameter)
%
%     nbIterApp : number + (learning parameter)
%
%     by Thomas Da Costa et Emeric Tonnelier 2014
  
  tailleTest = size(X, 1) / nbTest;
  nbClasse = size(unique(Y));
  if (tailleTest != round(tailleTest))
    printf("crossValidation_PMC : error : size(X, 1) / nbTest must be a round value\n");
    return;
  endif
  perfs = -ones(nbTest, 1);
  perfMoyenneParClasse = -ones(nbTest, nbClasse);
  perfEcartTypeParClasse = -ones(1, nbClasse);
  for i = 1 : nbTest
    printf("test %d/%d\n", i, nbTest);
    fflush(stdout);
    firstIndiceTest = (i * tailleTest) - tailleTest;
    if (firstIndiceTest == 0)
      firstIndiceTest = 1;
    endif
    xapp = [X([1:firstIndiceTest], :); X([firstIndiceTest+tailleTest:end], :)];
    yapp = [Y([1:firstIndiceTest], :); Y([firstIndiceTest+tailleTest:end], :)];
    xtest = X([firstIndiceTest:firstIndiceTest+tailleTest],:);
    ytest = Y([firstIndiceTest:firstIndiceTest+tailleTest],:);
    printf("train\n", i, nbTest);
    fflush(stdout);
    w = learn_PMC(xapp, yapp, epsylon, nbIterApp);
    printf("Predict\n", i, nbTest);
    fflush(stdout);
    yPred = estimation_PMC(xtest, w);
    perfs(i, 1) = mean(ytest == yPred);
    for class = 1 : nbClasse
      masque = yPred == ytest;
      perfMoyenneParClasse(i, class) = sum(yPred(masque == 1) == class) / sum(yPred == class);
      perfEcartTypeParClasse(1, class) = std(perfMoyenneParClasse([1:i], class));
    endfor
  endfor
  perfMoyenne = mean(perfs);
  perfEcartType = std(perfs);
  printf("training over all the dataset\n");
  fflush(stdout);
  w = learn_PMC(X, Y, epsylon, nbIterApp);
endfunction