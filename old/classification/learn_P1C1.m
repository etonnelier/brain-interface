function [w, combinaisons] = learn_P1C1(xapp, yapp, epsilon, nIter)

%     learn data X caracteristics acording to y labels
%     Perceptron model
%     make it iterations throught data.
%     return a w model
%
%     xapp : [experience; caracteristics]
%
%     yapp : [experience; Classe]
%
%     epsilon : number +
%
%     nIter : number +
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  nbClass = size(unique(yapp), 1);
  nbCaract = size(xapp, 2);
  nbExperiences = size(xapp, 1);
  combinaisons = kParmisN(2, nbClass);
  tmp = combinaisons(:,1);
  combinaisonsInv = combinaisons;
  combinaisonsInv(:,1) = combinaisonsInv(:,2);
  combinaisonsInv(:,2) = tmp;
  combinaisons = [combinaisons; combinaisonsInv];
  nbCombinaison = size(combinaisons, 1);
  w = randn(nbCaract, nbCombinaison);
  for combinaison = 1 : nbCombinaison
%    keyboard();
    classe1 = combinaisons(combinaison, 1);
    classe2 = combinaisons(combinaison, 2);
    mxapp = xapp(yapp == classe1, :);
    myapp = ones(size(mxapp, 1), 1);
    mxapp2 = xapp(yapp == classe2, :);
    myapp2 = -ones(size(mxapp2, 1), 1);
    mxapp = [mxapp; mxapp2];
    myapp = [myapp; myapp2];
    
    s = size(mxapp, 1) - 1;
    for it = 1 : nIter
      index = round(s * rand() + 1);
      xi = mxapp(index, :);
      yi = myapp(index, :);
      if (yi * xi * w(:, combinaison)) <= 0
        w(:, combinaison) = w(:, combinaison) + epsilon * xi' * yi;
      endif
    endfor
  endfor
endfunction