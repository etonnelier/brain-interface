function [perfMoyenne, perfEcartType, perfs, perfMoyenneParClasse, perfEcartTypeParClasse, w] = crossValidation_libsvm(X, Y, nbTest, lerningParameters)

%     perform an cross validation on the X data and Y labels.
%     LibSVM classification
%     return statistics about performances
%
%     X : [experiences; caracteristics]
%
%     Y : [experiences; labels]
%
%     nbTest : number : number of test for validation : ! size(X, 1) / nbTest must be a round value
%
%     lerningParameters : String libsvm_options (see : http://www.csie.ntu.edu.tw/~cjlin/libsvm/)
%
%     by Thomas Da Costa et Emeric Tonnelier 2014
  
  tailleTest = size(X, 1) / nbTest;
  nbClasse = size(unique(Y));
  if (tailleTest != round(tailleTest))
    printf("crossValidation_libsvm : error : size(X, 1) / nbTest must be a round value\n");
    return;
  endif
  perfs = -ones(nbTest, 1);
  perfMoyenneParClasse = -ones(nbTest, nbClasse);
  perfEcartTypeParClasse = -ones(1, nbClasse);
  for i = 1 : nbTest
    printf("test %d/%d\n", i, nbTest);
    fflush(stdout);
    firstIndiceTest = (i * tailleTest) - tailleTest;
    if (firstIndiceTest == 0)
      firstIndiceTest = 1;
    endif
    xapp = [X([1:firstIndiceTest], :); X([firstIndiceTest+tailleTest:end], :)];
    yapp = [Y([1:firstIndiceTest], :); Y([firstIndiceTest+tailleTest:end], :)];
    xtest = X([firstIndiceTest:firstIndiceTest+tailleTest],:);
    ytest = Y([firstIndiceTest:firstIndiceTest+tailleTest],:);
    printf("train\n", i, nbTest);
    fflush(stdout);
    w = svmtrain(yapp, xapp, lerningParameters);
    printf("Predict\n", i, nbTest);
    fflush(stdout);
    [predicted_label, accuracy, decision_values] = svmpredict(ytest, xtest, w);
    perfs(i, :) = max(accuracy);
    for class = 1 : nbClasse
      masque = predicted_label == ytest;
      perfMoyenneParClasse(i, class) = sum(predicted_label(masque == 1) == class) / sum(predicted_label == class);
      perfEcartTypeParClasse(1, class) = std(perfMoyenneParClasse([1:i], class));
    endfor
  endfor
  perfMoyenne = mean(perfs);
  perfEcartType = std(perfs);
  printf("training over all the dataset\n");
  fflush(stdout);
  w = svmtrain(Y, X, lerningParameters);
endfunction