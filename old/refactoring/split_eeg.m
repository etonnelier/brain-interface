function x = split_eeg(eeg, interval)
  x = eeg(:, :, :, interval);
  s = size(x);
  l = length(s);
  x = reshape(x, prod(s), 1);
  x = reshape(x, prod(s(1 : (l - 1))), s(l))';
endfunction
