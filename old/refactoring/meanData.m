function [dataMoy] = meanData(data, windowSize)

%     return new data vector with mean values of original data
%
%     data : [experience; captors; caracteristics]
%
%     windowSize : Integer. caracteristics / windowSize must be a round value
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  newSize = size(data, 3) / windowSize;
  pourcentSeuil = 1;
  if (round(newSize) != newSize)
    printf("meanData : error. windowSize / size(data, 3) must be a round value\n");
    return;
  endif
  ancV = pourcentSeuil;
  dataMoy = zeros(size(data, 1),size(data, 2),newSize);
  for i = 1 : size(data, 1)
    newV = mod((i / size(data, 1) * 100), pourcentSeuil);
    if (ancV > newV)
      printf("%d%% %d/%d\n", i/size(data, 1)*100, i, size(data, 1));
      fflush(stdout);
    endif
    ancV = newV;
    for j = 1 : size(data, 2)
      for k = 1 : size(data, 3) - windowSize
        if mod(k, windowSize) == 0
          dataMoy(i, j, (k/windowSize) + 1) = mean(data(i,j,[k:k+windowSize]));
        endif
      endfor
    endfor
  endfor
  dataMoy = dataMoy(:,:,[2:end]);
endfunction