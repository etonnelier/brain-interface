function [dataNormalized] = normalizeData(data)
%  
%     Normalize values from a 2-dim data matrix between 0 and 1.
%     Keep the sign
%
%     data : [n*m] double
%
%     by Thomas Da Costa and Emeric Tonnelier 2014
  mask = data < 0;
  dataNormalized = abs(data);
  dataMin = min(min(dataNormalized));
  dataMax = max(max(dataNormalized));
  dataNormalized = dataNormalized - dataMin;
  dataNormalized = dataNormalized / dataMax;
  dataNormalized(mask == 1) *= -1;
endfunction