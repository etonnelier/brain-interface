function [dataFilter] = filter_passe_bas(data, cutOffFrequency, nbCapteur, nbTemps)
  % The sampling frequency in Hz.
  Fsam = nbTemps/.5;
  % Nyquist frequency, in Hz.
  % The Nyquist frequency is half your sampling frequency.
  Fnyq = Fsam/2;
  if (cutOffFrequency <= 0 || cutOffFrequency > Fnyq)
    printf("filter_passe_bas : error : wrong cutOffFrequency\n");
    return;
  endif
  Fc = cutOffFrequency;
  % Create a first-order Butterworth low pass.
  % The returned vectors are of legth n.
  % Thus a first order filter is created with n = 2.
  n=2;
  [b,a]=butter(n, Fc/Fnyq);
  
  sampl = 25;     % réglage du sous-échantillonnage  
  ntn = length([1:nbTemps](1:sampl:end));
  %% allocation de la mémoire pour ganger du temps
  dataFilter = zeros(size(data,1), size(ntn*nbCapteur,2));
  
  for i=1:size(data,1) % pour tous les échantillons
    for j=1:nbCapteur % pour tous les cannaux
        % filtrage + souséchantillonage
      dataFilter(i,(j-1)*ntn+1:j*ntn) = filter(b,a,data(i,(j-1)*nbTemps+1:j*nbTemps))(1:sampl:end);
    end
  end
endfunction