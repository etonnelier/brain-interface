function [X, Y] = shakeExperiences(X, Y)

%     return caracteristics and labels vector in a random order
%
%     X : [experience; caracteristics]
%
%     Y : [exprience; labels]
%
%     by Thomas Da Costa et Emeric Tonnelier 2014

  if (size(X, 1) != size(Y, 1))
    printf("shakeExperiences : error : size(X, 1) must be equal to size(Y, 1)\n");
    return;
  endif
  longueur = size(X, 1);
  indices = [1:longueur];
  permutations = zeros(1, longueur);

  for i = 1 : longueur
    indice = round(rand() * (length(indices)-1))+1;
    permutations(1, i) = indices(indice);
 	  indices = [indices(1:indice-1), indices(indice+1:length(indices))];
  endfor

  X = X(permutations, :);
  Y = Y(permutations, :);
endfunction