function [X, Y] = split_eeg_to_XY(data)
##
## Split data from EEG to structured form
## Delete experiences with NaN value(s)
## 
## data : [individuals; letters; captors; times; experiences]
##
## X : [experiences; captors * times]
##
## Y : [experiences; 2], first column individual, second column letter
##
## By Thomas Da Costa et Emeric Tonnelier 2014

dataSize = size(data);
nbIndividus = dataSize(1);
nbLettres = dataSize(2);
nbCaptor = dataSize(3);
nbTime = dataSize(4);
nbExperience = dataSize(5);

for personne = 1 : 3
  for letter = 1 : nbLettres
    newData = data(personne, letter, :, :, :);
    newData = reshape(newData, nbCaptor, nbTime, nbExperience);
    errors = reshape(sum(sum(isnan(newData), 2), 1), nbExperience, 1);
    newData = newData(:, :, errors == 0);
    nbExperienceFinale = size(newData, 3);
    newData = permute(newData, [3, 1, 2]);
    newResult = zeros(nbExperienceFinale, 2);
    newResult(:, 1) = personne;
    newResult(:, 2) = letter;
    if (personne == 1 && letter == 1) 
      X = newData;
      Y = newResult;
    else
      X = [X; newData];
      Y = [Y; newResult];
    endif
  endfor
endfor

X = reshape(X, size(X, 1), size(X,2) * size(X,3));

endfunction
