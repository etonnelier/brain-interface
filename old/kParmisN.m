function [combi] = kParmisN(k, n)
  combi = [1:k];
  i = 1;
  nbCombi = (factorial(n) / ((factorial(k) * factorial(n - k))));
  while(i < nbCombi)
    ind = k;
    newLine = combi(end, :);
    newLine(ind) += 1;
    if (newLine(ind) > n)
      modif = 1;
      while modif
        ind -= 1;
        if (newLine(ind) + 1 + (k - ind) <= n)
          newLine(ind) += 1;
          newLine([ind+1:end]) = [newLine(ind)+1:newLine(ind)+1+(k-ind-1)];
          modif = 0;
        endif
      endwhile
    endif
    combi = [combi; newLine];
    i++;
  endwhile
endfunction