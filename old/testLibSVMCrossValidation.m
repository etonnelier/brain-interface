addpath("./data", "./classification", "./classification/libsvm-3.17/matlab", "./refactoring");
clear all;
close all;

%load("donnees_test_split_moy.mat");
%load("donnees_splitSelect_fpb.mat");
load("donneesSelectMoy.mat");

dimensionTest = 1;

%X = [xapp; xtest([1:end-33],:)];
X = X([1:end-33],:);
%Y = [yapp(:,dimensionTest); ytest([1:end-33],dimensionTest)];
Y = Y([1:end-33],dimensionTest);

[X, Y] = shakeExperiences(X, Y);

[perfMoyenne, perfEcartType, perfs, perfMoyenneParClasse, perfEcartTypeParClasse, w] = crossValidation_libsvm(X, Y, 7, "-s 1 -q -t 0");

figure;
bar(perfMoyenneParClasse);
title("Performances moyennes par classe et experiences - libsvm");
xlabel("experiences");
ylabel("performance");


rmpath("./data", "./classification", "./classification/libsvm-3.17/matlab", "./refactoring");