## -*- texinfo -*- ##
## TODO !

function handles = getHandles(varargin)
  n = 1;
  flag = false;
  for i = 1 : numel(varargin)
    if(is_function_handle(varargin{i}))
      if(n > 1 && flag)
	handles{3, n - 1} = i - 1;
      endif
      handles{1, n} = varargin{i};
      handles{2, n} = i + 1;
      handles{3, n} = 0;
      n++;
      flag = false;
    else
      flag = true;
    endif
  endfor
  if(flag)
    handles{3, n - 1} = numel(varargin);
  endif
endfunction
