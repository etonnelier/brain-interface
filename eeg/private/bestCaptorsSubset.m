## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{bestSubSet} =} bestCaptorsSubset(@var{eeg}, @var{label}, @var{nbCapteursWanted}, @dots{})
## Return a structure @var{bestSubSet} of the @var{nbCapteursWanted} bestTested captors by crossValidation
## with learn and test functions on an eegObject on the label @var{label}.
##
##
## Usage:
##
## @example
## bestSubset = bestCaptorsSubset(eegApp, 1, 60, @learnPerceptron1Vs1, 0.01, 5000, @predictPerceptron1Vs1);
## @end example
## @end deftypefn

function [bestSubSet] = bestCaptorsSubset(eeg, label, nbCapteursWanted, varargin)
	if(nargin < 2)
    	print_usage();
  	elseif(strcmp(class(eeg), "eegObject") == 0)
    	error("bestCaptorsSubset: first argument isn't an eegObject");
  	elseif(isreal(label) == 0)
    	error("bestCaptorsSubset: second argument isn't a real")
    end
    fnct = getHandles(varargin{:});
    if(size(fnct, 2) < 2)
    	error("bestCaptorsSubset: expected two function handles")
  	end
  	y = eeg.labels;
  	nbLabelsTypes = size(y, 2);
  	if(label < 1 || label > nbLabelsTypes)
    	error("bestCaptorsSubset: label has to be in [1..%d], given %d",
	  	nbLabelsTypes,
		label);
	end
	if (nbCapteursWanted > eeg.nbSensors || nbCapteursWanted < 1)
		error("bestCaptorsSubset: nbCapteursWanted must be larger than eeg number of captors");
	end
	y = eeg.labels;
	y = y(:,label);
	nbClasses = size(unique(y), 1);
	nbCaptors = eeg.nbSensors;
	eeg = shuffle(eeg);
	for i = 1 : nbClasses
		for j = i+1 : nbClasses
			bestSubSet{i,j} = [1:nbCaptors];
		end
	end
	while(nbCaptors > nbCapteursWanted)
    results = [];
		for i = 1 : nbCaptors
			for j = 1 : nbClasses
				for k = j+1 : nbClasses
					capteursVoulus = bestSubSet{j,k};
					if i == 1
						eegTmp = getCaptors(eeg, capteursVoulus([2:nbCaptors]));
					elseif i == nbCaptors
						eegTmp = getCaptors(eeg, capteursVoulus([1:nbCaptors-1]));
					else
						eegTmp = getCaptors(eeg, capteursVoulus([[1:i-1],[i+1:nbCaptors]]));
					end
          masqueJ = y(:,1) == j;
          masqueK = y(:,1) == k;
          masque = masqueJ + masqueK;
					perfeeg = eegObject(nbCaptors-1, eeg.nbTimes, ...
						eegTmp.base(masque == 1, :), eegTmp.labels(masque == 1, :));
%					perf = crossValidation(perfeeg, label, 3, fnct{1, 1}, ...
%						varargin{fnct{2, 1} : fnct{3, 1}}, fnct{1, 2}, varargin{fnct{2, 1} : fnct{3, 2}});
          sizeApp = round(size(perfeeg.base, 1)*0.7);
          [eegApp, eegTest] = split(perfeeg, [1:sizeApp]);
          model = learnPerceptron1Vs1(eegApp, label, 0.01, 10000);
          [yhat, perf] = predictPerceptron1Vs1(eegTest, model);
					results(j, k, i) = mean(perf);
          disp(sprintf("NbCapteurs %d del capteur %d class %d contre %d -> mean : %f", nbCaptors, i, j, k, mean(perf)));
          fflush(stdout);
				end
			end
		end
		for i = 1 : nbClasses
			for j = i+1 : nbClasses
				[t, bestCaptors] = sort(results(i, j, :));
				bestCaptors = bestCaptors(end);
				if (bestCaptors == 1)
					bestCaptors = bestSubSet{i,j}([2:nbCaptors]);
				elseif (bestCaptors == nbCaptors)
					bestCaptors = bestSubSet{i,j}([1:nbCaptors-1]);
				else
					bestCaptors = bestSubSet{i,j}([[1:bestCaptors-1], [bestCaptors+1:nbCaptors]]);
				end
				bestSubSet{i, j} = bestCaptors;
			end
		end
		nbCaptors--;
	end
end
