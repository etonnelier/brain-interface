## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{bestSubSet} =} bestTimeSubset(@var{eeg}, @var{label}, @var{nbTimesWanted}, @dots{})
## Return a structure @var{bestSubSet} of the @var{nbTimesWanted} bestTested times by crossValidation
## with learn and test functions on an eegObject on the label @var{label}.
##
##
## Usage:
##
## @example
## bestSubset = bestTimeSubset(eegApp, 1, 20, @learnPerceptron1Vs1, 0.01, 5000, @predictPerceptron1Vs1);
## @end example
## @end deftypefn

function [bestSubSet] = bestTimeSubset(eeg, label, nbTimesWanted, varargin)
	if(nargin < 2)
    	print_usage();
  	elseif(strcmp(class(eeg), "eegObject") == 0)
    	error("bestTimeSubset: first argument isn't an eegObject");
  	elseif(isreal(label) == 0)
    	error("bestTimeSubset: second argument isn't a real")
    end
    fnct = getHandles(varargin{:});
    if(size(fnct, 2) < 2)
    	error("bestTimeSubset: expected two function handles")
  	end
  	y = eeg.labels;
  	nbLabelsTypes = size(y, 2);
  	if(label < 1 || label > nbLabelsTypes)
    	error("bestTimeSubset: label has to be in [1..%d], given %d",
	  	nbLabelsTypes,
		label);
	end
	if (nbTimesWanted > eeg.nbTimes || nbTimesWanted < 1)
		error("bestTimeSubset: nbTimesWanted must be larger than eeg number of times");
	end
	y = eeg.labels;
	y = y(:,label);
	nbClasses = size(unique(y), 1);
	nbTimes = eeg.nbTimes;
	eeg = shuffle(eeg);
	for i = 1 : nbClasses
		for j = i+1 : nbClasses
			bestSubSet{i,j} = [1:nbTimes];
		end
	end
	while(nbTimes > nbTimesWanted)
    	results = zeros(nbClasses, nbClasses, nbTimes);
		for i = 1 : nbTimes
			for j = 1 : nbClasses
				for k = j+1 : nbClasses
					tempsVoulus = bestSubSet{j,k}([1:nbTimes]);
					if i == 1
						eegTmp = getTimes(eeg, tempsVoulus([2:nbTimes]));
					elseif i == nbTimes
						eegTmp = getTimes(eeg, tempsVoulus([1:nbTimes-1]));
					else
						eegTmp = getTimes(eeg, tempsVoulus([[1:i-1],[i+1:nbTimes]]));
					end
          masqueJ = y(:,1) == j;
          masqueK = y(:,1) == k;
          masque = masqueJ + masqueK;
					perfeeg = eegObject(eegTmp.nbSensors, nbTimes-1, ...
						eegTmp.base(masque == 1, :), eegTmp.labels(masque == 1, :));
					perf = crossValidation(perfeeg, label, 3, fnct{1, 1}, ...
					varargin{fnct{2, 1} : fnct{3, 1}}, fnct{1, 2}, varargin{fnct{2, 1} : fnct{3, 2}});
          disp(sprintf("NbTimes %d del time %d class %d contre %d -> mean : %f", nbTimes, i, j, k, mean(perf)));
          fflush(stdout);
					results(j, k, i) = mean(perf);
				end
			end
		end
		for i = 1 : nbClasses
			for j = i+1 : nbClasses
				[t, bestTimes] = sort(results(i, j, :));
				bestTimes = bestTimes(end);
				if (bestTimes != nbTimes)
					tmp = bestSubSet{i,j}(bestTimes);
					bestSubSet{i,j}(bestTimes) = bestSubSet{i,j}(nbTimes);
					bestSubSet{i,j}(nbTimes) = tmp;
				end
			end
		end
		nbTimes--;
	end
end
