## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{r}] =} deuxParmis(@var{n})
## Return uniques combinaisons of 2 element along n elements
##
## Usage:
##
## @example
## r = deuxParmis(5)
## @end example
## @end deftypefn
function [r] = deuxParmi(n)
r = [];
for i = 1 : n
  for j = i+1 : n
    r = [[i,j];r];
  end
end
