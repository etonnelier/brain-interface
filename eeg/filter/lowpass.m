## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{e} =} lowpass(@var{eeg}, @var{kernel})
## Apply a low-pass filter define by the vector @var{kernel}
## on each sensor signal of an eegObject and return a new eegObject @var{e}.
##
## @var{kernel} is used as it is given, in most cases sum of its
## elements should be equal to 1.
## @end deftypefn

function result = lowpass(eeg, kernel)
  if(nargin != 2)
    print_usage();
  endif
  if(strcmp(class(eeg), "eegObject") == 0)
    error("lowpass: first argument isn't an eegObject");
  elseif(isvector(kernel) == 0)
    error("lowpass: second argument isn't a vector");
  endif
  if(abs(sum(kernel) - 1) > 1e-3)
    warning("lowpass: sum(kernel) == %g", sum(kernel));
  endif
  kernel = reshape(kernel, 1, length(kernel));
  nbSensors = eeg.nbSensors;
  nbTimes = eeg.nbTimes;
  labels = eeg.labels;
  base = eeg.base;
  nbExperiences = size(base, 1);
  base = reshape(base', nbTimes, nbSensors * nbExperiences)';
  newNbTimes = nbTimes - length(kernel) + 1;
  for pos = 1 : newNbTimes
    p = bsxfun(@times, base(:, pos:pos+length(kernel)-1), kernel);
    res(:, pos) = sum(p, 2);
    fprintf("\rlowpass processing: %6.2f%%",  (pos / newNbTimes) * 100);
    fflush(stdout);
  endfor
  fprintf("\n");
  base = reshape(res', newNbTimes * nbSensors, nbExperiences)';
  result = eegObject(nbSensors, newNbTimes, base, labels);
endfunction
