## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{v} =} subsref(@var{eeg}, @var{s})
## Index the contents of an object eegObject.
## @end deftypefn

function v = subsref(eeg, s)
  if(isempty(s))
    error("eegObject: missing index");
  end
  switch(s(1).type)
    case "."
      v = get(eeg, s(1).subs);
    otherwise
      error("eegObject: invalid subscript type");
  end
  if(numel(s) > 1)
    v = subsref(v, s(2:end));
  end
end
