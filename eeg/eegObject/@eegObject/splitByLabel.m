## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{e1}, @var{e2}] =} splitByLabel(@var{eeg}, @var{l}, @var{v})
## Split an object eegObject by the given label type @var{l} and @var{v})
## the value of this label.
## return two new eegObject @var{e1} and @var{e2}.
## @end deftypefn

function [part1, part2] = splitByLabel(eeg, label, value)
  if(nargin != 3)
    print_usage();
  end
  if(strcmp(class(eeg), "eegObject") == 0)
    error("splitByLabel: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("splitByLabel: second argument isn't a real");
  elseif(label > size(eeg.labels, 2) || label < 1)
    error("splitByLabel: label is out of bounds; %d out of bound %d",
	  label,
	  size(eeg.labels, 2));
  end
  interval = (eeg.labels(:, label) == value);
  [part1, part2] = split(eeg, interval);
end
