## -*- texinfo -*- ##
## @deftypefn {Function File} eeg(@var{s}, @var{t}, @var{x}, @var{y})
## Create an eeg object with
## @var{s} as the number of sensors,
## @var{t} the number of time steps,
## @var{b} the experiences base matrix [n x (@var{s} * @var{t})] and
## @var{l} the labels matrix [n x m].
## @end deftypefn

function eeg = eegObject(s, t, b, l)
  if(nargin == 0)
    eeg.nbSensors = 0;
    eeg.nbTimes = 0;
    eeg.base = [];
    eeg.labels = [];
    eeg = class(eeg, "eegObject");
  elseif(nargin == 4)
    if(isreal(s) && isreal(t) && ismatrix(b) && ismatrix(l))
      if(length(size(b)) != 2 && length(size(l)) != 2)
        error("eegObject: given matrices aren't two-dimensional");
      elseif(size(b, 1) != size(l, 1))
        error("eegObject: rows(b)=%d != rows(l)=%d", size(b, 1), size(l, 1));
      elseif(s * t != size(b, 2))
        error("eegObject: %d*%d=%d expected as width of base matrix; given %d",
        s, t, s * t, size(b, 2));
      end
      eeg.nbSensors = s;
      eeg.nbTimes = t;
      eeg.base = b;
      eeg.labels = l;
      eeg = class(eeg, "eegObject");
    else
      print_usage();
    end
  else
    print_usage();
  end
end
