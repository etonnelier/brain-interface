## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{e} =} shuffle(@var{eeg})
## Permute randomly entries in experiences base matrix
## and labels matrix of an object eegObject and return a new eegObject @var{e}.
## @end deftypefn

function result = shuffle(eeg)
  if(nargin != 1)
    print_usage();
  endif
  if(strcmp(class(eeg), "eegObject") == 0)
    error("shuffle: first argument isn't an eegObject");
  end
  result = eeg;
  perm = randperm(size(result.base, 1));
  result.base = result.base(perm, :);
  result.labels = result.labels(perm, :);
end
