## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{eeg}] =} getCaptors(@var{eeg}, @var{captorNumber})
## Return an object eegObject @var{eeg} with only the given captors number @var{captorNumber}
## @end deftypefn


function [eeg] = getCaptors(eeg, captorNumber)
    if(nargin != 2)
        print_usage();
    end
    if(strcmp(class(eeg), "eegObject") == 0)
        error("getCaptor: first argument isn't an eegObject");
    elseif(isvector(captorNumber) == 0)
        error("getCaptor: second argument is't a number");
    elseif (eeg.nbSensors < max(captorNumber))
        error("getCaptor: captorNumber isn't in range [1:eeg.nbSensors]");
    elseif (min(captorNumber) <= 0)
        error("getCaptor: captorNumber can't be inferior or equal to 0");
    end
    for i = 1 : length(captorNumber)
        neeg(:, [((i-1)*eeg.nbTimes)+1:i*eeg.nbTimes]) = ...
            eeg.base(:,[((captorNumber(i)-1)*eeg.nbTimes)+1:captorNumber(i)*eeg.nbTimes]);
    end
    eeg = eegObject(length(captorNumber), eeg.nbTimes, neeg, eeg.labels);
end
