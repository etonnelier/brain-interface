## -*- texinfo -*- ##
## @deftypefn {Function File} display(@var{eeg})
## Display the contents of an object eegObject.
## @end deftypefn

function display(eeg)
  fprintf("%s =\n", inputname(1));
  fprintf("\tnbSensors = %d\n", eeg.nbSensors);
  fprintf("\tnbTimes = %d\n", eeg.nbTimes);
  fprintf("\tnbExperiences= %d\n", size(eeg.base, 1));
  fprintf("\tlabels =\n");
  for i = 1 : size(eeg.labels, 2)
    fprintf("\t\ttype %2d: %3d\n", i, length(unique(eeg.labels(:, i))));
  end
end
