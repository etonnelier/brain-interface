## -*- texinfo -*- ##
## @deftypefn {Function File} {[...] =} get(@var{eeg}, @dots{})
## Return properties from an object eegObject.
##
## Usage:
##
## @example
## [nbSensors, nbTimes] = get(eeg, "nbSensors", "nbTimes");
## @end example
## @end deftypefn

function varargout = get(eeg, varargin)
  for i = 1 : numel(varargin)
    property = varargin{i};
    if(ischar(property) == 0)
      error("eegObject: property isn't a string");
    end
    switch(property)
      case "nbSensors"
	varargout{i} = eeg.nbSensors;
      case "nbTimes"
	varargout{i} = eeg.nbTimes;
      case "base"
	varargout{i} = eeg.base;
      case "labels"
	varargout{i} = eeg.labels;
      case "nbExperiences"
	varargout{i} = size(eeg.base, 1);
      otherwise
	error("eegObject: invalid property \"%s\"", property);
    end
  end
end
