## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{e1}, @var{e2}] =} split(@var{eeg}, @var{i})
## Split an object eegObject with the given interval @var{i} and
## return two new eegObject @var{e1} and @var{e2}.
## @end deftypefn

function [part1, part2] = split(eeg, interval)
  if(nargin != 2)
    print_usage();
  end
  if(strcmp(class(eeg), "eegObject") == 0)
    error("split: first argument isn't an eegObject");
  elseif(isvector(interval) == 0)
    error("split: second argument isn't a vector");
  elseif(max(interval) > size(eeg.base, 1))
    error("split: inteval is out of bounds; %d out of bound %d",
	  max(interval),
	  size(eeg.base, 1));
  end
  if(strcmp(class(interval), "logical") == 1)
    diff = (interval == 0);
  else
    diff = setdiff(1 : size(eeg.base, 1), interval);
  end
  basePart1 = eeg.base(interval, :);
  basePart2 = eeg.base(diff, :);
  labelsPart1 = eeg.labels(interval, :);
  labelsPart2 = eeg.labels(diff, :);
  part1 = eegObject(eeg.nbSensors, eeg.nbTimes, basePart1, labelsPart1);
  part2 = eegObject(eeg.nbSensors, eeg.nbTimes, basePart2, labelsPart2);
end
