## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{eeg}] =} getTimes(@var{eeg}, @var{timesNumbers})
## Return an object eegObject @var{eeg} with only the given times number @var{timesNumbers}
## @end deftypefn


function [eeg] = getTimes(eeg, timesNumbers)
    if(nargin != 2)
        print_usage();
    end
    if(strcmp(class(eeg), "eegObject") == 0)
        error("getTimes: first argument isn't an eegObject");
    elseif(isvector(timesNumbers) == 0)
        error("getTimes: second argument is't a number");
    elseif (eeg.nbTimes < max(timesNumbers))
        error("getTimes: timesNumbers isn't in range [1:eeg.nbTimes]");
    elseif (min(timesNumbers) <= 0)
        error("getTimes: timesNumbers can't be inferior or equal to 0");
    end
    base = eeg.base;
    masque = zeros(1, eeg.nbTimes);
    masque(:, timesNumbers) = 1;
    masque = repmat(masque, 1, eeg.nbSensors);
    base = base(:, masque == 1);
    eeg = eegObject(eeg.nbSensors, length(timesNumbers), base, eeg.labels);
end
