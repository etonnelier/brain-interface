## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{model}] =} learnPerceptron1Vs1TimeSelection(@var{eeg}, @var{label}, @var{epsilon}, @var{nIter}, @var{subSet}, @var{nbTemps})
## Return a object @var{model} which is a '1 vs 1' perceptron learning model
## for an eegObject @var{eeg} on the label @var{label} and a @var{epsilon} modification step.
## model is learn whith the @var{nbTemps} times from @var{subSet}
##
## Usage :
##
## @example
## model = learnPerceptron1Vs1TimeSelection(eeg, 1, 0.01, 1000, subSet, 20);
## @end example
## @end deftypefn

function model = learnPerceptron1Vs1TimeSelection(eeg, label, epsilon, nIter, subSet, nbTemps)
  if(nargin != 6)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("learnPerceptron1Vs1TimeSelection: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("learnPerceptron1Vs1TimeSelection: second argument isn't a real");
  elseif(isreal(epsilon) == 0)
    error("learnPerceptron1Vs1TimeSelection : third argument isn't a real");
  elseif(isreal(nIter) == 0)
    error("learnPerceptron1Vs1TimeSelection : fourth argument isn't a real");
  end
  yapp = eeg.labels;
  nbLabelsTypes = size(yapp, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("learnPerceptron1Vs1TimeSelection: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  end
  yapp = yapp(:,label);
  nbClasses = size(unique(yapp), 1);
  if (size(subSet, 1) != size(subSet, 2)-1 || size(subSet, 2) != nbClasses)
    error("learnPerceptron1Vs1TimeSelection: subSet not compatible");
  end
  combinaisons = deuxParmi(nbClasses);
  nbCombinaisons = size(combinaisons, 1);
  w = zeros(eeg.nbSensors*nbTemps, nbCombinaisons);
  for i = 1 : nbCombinaisons
    classe1 = combinaisons(i, 1);
    classe2 = combinaisons(i, 2);
    temps = subSet{classe1, classe2}([1:nbTemps]);
    xapp = getTimes(eeg, temps).base;
    mxapp = xapp(yapp == classe1, :);
    myapp = ones(size(mxapp, 1), 1);
    mxapp2 = xapp(yapp == classe2, :);
    myapp2 = -ones(size(mxapp2, 1), 1);
    mxapp = [mxapp; mxapp2];
    myapp = [myapp; myapp2];
    s = size(mxapp, 1) - 1;
    for it = 1 : nIter
      index = round(s * rand() + 1);
      xi = mxapp(index, :);
      yi = myapp(index, :);
      if (yi * xi * w(:, i)) <= 0
        w(:, i) = w(:, i) + epsilon * xi' * yi;
      endif
    endfor
  endfor
  myModel.model = w;
  myModel.combinaisons = combinaisons;
  myModel.timesNumber = subSet;
  myModel.nbTimes = nbTemps;
  model = modelObject("perceptron1VS1TimeSelection", label, eeg.nbTimes, eeg.nbSensors, myModel);
endfunction
