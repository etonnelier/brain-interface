## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{w} =} learnBasic(@var{eeg}, @var{l})
## Return a matrix @var{w} which is a basic '1 vs ALL' learning model
## for an eegObject on the label @var{l}.
##
## This method use the '\' operator of Ocatve.
## @end deftypefn

function w = learnBasic(eeg, label)
  if(nargin != 2)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("learnBasic: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("learnBasic: second argument isn't a real")
  end
  x = eeg.base;
  y = eeg.labels;
  nbLabelsTypes = size(y, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("learnBasic: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  end
  y = y(:, label);
  classes = unique(y);
  w = [];
  for i = 1 : length(classes)
    c = classes(i, 1);
    ytmp = y;
    diff = ytmp == c;
    ytmp(diff, 1) = 1;
    ytmp(diff == 0, 1) = -1;
    warning("off", "Octave:singular-matrix-div");
    w(:, i) = (x' * x) \ (x' * ytmp);
    warning("on", "Octave:singular-matrix-div");
  end
  w = modelObject("basic", label, eeg.nbTimes, eeg.nbSensors, w);
end
