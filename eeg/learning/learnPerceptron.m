## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{w}] =} learnPerceptron(@var{eeg}, @var{label}, @var{epsilon}, @var{nIter})
## Return a matrix @var{w} which is a '1 vs ALL' perceptron learning model
## for an eegObject @var{eeg} on the label @var{label} and a @var{epsilon} modification step.
##
## Usage :
##
## @example
## model = learnPerceptron(eeg, 1, 0.01, 1000);
## @end example
## @end deftypefn

function model = learnPerceptron(eeg, label, epsilon, nIter)
  if(nargin != 4)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("learnPerceptron: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("learnPerceptron: second argument isn't a real");
  elseif(isreal(epsilon) == 0)
    error("learnPerceptron : third argument isn't a real");
  elseif(isreal(nIter) == 0)
    error("learnPerceptron : fourth argument isn't a real");
  endif
  xapp = eeg.base;
  yapp = eeg.labels;
  nbLabelsTypes = size(yapp, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("learnBasic: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  endif
  yapp = yapp(:,label);
  nbClasses = size(unique(yapp), 1);
  w = zeros(size(xapp, 2), nbClasses);
  s = size(xapp, 1) - 1;
  for i = 1 : nIter
    index = round(s * rand() + 1);
    xi = xapp(index, :);
    for classes = 1 : nbClasses
      if (yapp(index) == classes)
        yi=1;
      else
        yi=-1;
      endif
      if (yi * xi * w(:, classes)) <= 0
        w(:, classes) = w(:, classes) + epsilon * xi' * yi;
      endif
    endfor
  endfor
  model = modelObject("perceptron1VSAll", label, eeg.nbTimes, eeg.nbSensors, w);
endfunction
