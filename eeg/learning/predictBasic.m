## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{y}, @var{s}] =} predictBasic(@var{eeg}, @var{w})
## Return predicted labels @var{y} and percentage of succes @var{s} of an eegObject
## with a basic '1 vs ALL' learning model @var{w}.
##
## @var{s} is the succes of prediction between 0 and 1.
## @end deftypefn

function [yHat, s] = predictBasic(eeg, w)
  if(nargin != 2)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("predictBasic: first argument isn't an eegObject");
  elseif(strcmp(class(w), "modelObject") == 0)
    error("predictBasic: second argument isn't an modelObject");
  elseif(istype(w, "basic") == 0)
    error("predictBasic: second argument isn't a basic model")
  endif
  x = eeg.base;
  y = eeg.labels;
  label = w.labels;
  nbLabelsTypes = size(y, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("predictBasic: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  elseif(size(w.model, 1) != size(x, 2))
    error("predictBasic: model and eegObject aren't compatible");
  endif
  y = y(:, label);
  yhat = (x * w.model);
  [m, idx] = max(yhat');
  classes = unique(y);
  for i = 1 : length(classes)
    r(i, :) = (idx == i);
  endfor
  for i = 1 : length(classes)
    idx(r(i, :)) = classes(i, 1);
  endfor
  yHat = idx';
  s = mean(y == yHat);
endfunction
