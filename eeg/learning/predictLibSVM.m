## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{yHat}, @var{s}] =} predictLibSVM(@var{eeg}, @var{model})
## Return predicted labels @var{yHat} of an eegObject on the label type @var{label}
## with a libSVM learning model @var{model}.
##
## @var{s} is the succes of prediction between 0 and 1.
##
## Note :
## LibSVM mathlab must be installed on the system to use libSVM features.
## See http://www.csie.ntu.edu.tw/~cjlin/libsvm/ for more informations about libsvm.
## Methods must be accessible in the classpath.
## you can use addpath or rmpath.
##
## Usage :
##
## @example
## [yHat, s] = predictLibSVM(eeg, model);
## @end example
## @end deftypefn

function [yHat, s] = predictLibSVM(eeg, model)
  if(nargin != 2)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("predictLibSVM: first argument isn't an eegObject");
  elseif(strcmp(class(model), "modelObject") == 0)
    error("predictLibSVM: second argument isn't a modelObject")
  elseif(istype(model, "libSVM") == 0)
    error("predictLibSVM: second argument isn't a valid modelObject")
  endif
  X = eeg.base;
  Y = eeg.labels;
  w = model.model;
  label = model.labels;
  nbLabelsTypes = size(Y, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("predictLibSVM: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  endif
  [yHat, s, decision_values] = svmpredict(Y, X, w);
  s = max(s) / 100;
endfunction
