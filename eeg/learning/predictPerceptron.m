## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{yHat}, @var{s}] =} predictPerceptron(@var{eeg}, @var{model})
## Return predicted labels @var{yHat} of an eegObject on the label type @var{label}
## with a '1 vs ALL' perceptron learning model @var{w}.
##
## @var{s} is the succes of prediction between 0 and 1.
##
## Usage :
##
## @example
## w = learnPerceptron(eeg, model, 0.3, 7000);
## @end example
## @end deftypefn

function [yHat, s] = predictPerceptron(eeg, model)
  if(nargin != 2)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("predictPerceptron: first argument isn't an eegObject");
  elseif(strcmp(class(model), "modelObject") == 0)
    error("predictPerceptron: second argument isn't a modelObject");
  elseif(istype(model, "perceptron1VSAll") == 0)
    error("predictPerceptron: second argument isn't a valid modelObject");
  endif
  X = eeg.base;
  Y = eeg.labels;
  w = model.model;
  label = model.labels;
  nbLabelsTypes = size(Y, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("predictPerceptron: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  elseif(size(w, 1) != size(X, 2))
    error("predictPerceptron: model and eegObject aren't compatible");
  endif
  Y = Y(:, label);
  nbClasses = size(w, 1);
  results = X * w;
  [maxValue, yHat] = max(results, [], 2);
  classes = unique(Y);
  for i = 1 : length(classes)
    r(i, :) = (yHat == i);
  endfor
  for i = 1 : length(classes)
    yHat(r(i, :)) = classes(i, 1);
  endfor
  s = mean(Y == yHat);
endfunction
