## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{model}] =} learnPerceptron1Vs1(@var{eeg}, @var{label}, @var{epsilon}, @var{nIter})
## Return a object @var{model} which is a '1 vs 1' perceptron learning model
## for an eegObject @var{eeg} on the label @var{label} and a @var{epsilon} modification step.
##
## Usage :
##
## @example
## model = learnPerceptron1Vs1(eeg, 1, 0.01, 1000);
## @end example
## @end deftypefn

function model = learnPerceptron1Vs1(eeg, label, epsilon, nIter)
  if(nargin != 4)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("learnPerceptron: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("learnPerceptron: second argument isn't a real");
  elseif(isreal(epsilon) == 0)
    error("learnPerceptron : third argument isn't a real");
  elseif(isreal(nIter) == 0)
    error("learnPerceptron : fourth argument isn't a real");
  end
  xapp = eeg.base;
  yapp = eeg.labels;
  nbLabelsTypes = size(yapp, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("learnPerceptron: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  end
  yapp = yapp(:,label);
  nbClasses = length(unique(yapp));
  combinaisons = deuxParmi(nbClasses);
  nbCombinaisons = size(combinaisons, 1);
  w = zeros(size(xapp, 2), nbCombinaisons);
  for i = 1 : nbCombinaisons
    classe1 = combinaisons(i, 1);
    classe2 = combinaisons(i, 2);
    mxapp = xapp(yapp == classe1, :);
    myapp = ones(size(mxapp, 1), 1);
    mxapp2 = xapp(yapp == classe2, :);
    myapp2 = -ones(size(mxapp2, 1), 1);
    mxapp = [mxapp; mxapp2];
    myapp = [myapp; myapp2];
    s = size(mxapp, 1) - 1;
    for it = 1 : nIter
      index = round(s * rand() + 1);
      xi = mxapp(index, :);
      yi = myapp(index, :);
      if (yi * xi * w(:, i)) <= 0
        w(:, i) = w(:, i) + epsilon * xi' * yi;
      end
    end
  end
  myModel.model = w;
  myModel.combinaisons = combinaisons;
  model = modelObject("perceptron1VS1", label, eeg.nbTimes, eeg.nbSensors, myModel);
endfunction
