## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{p}, @var{oe}, @var{oit}] =} optimizePerceptron(@var{eeg}, @var{l}, @var{e}, @var{it})
## Return best performance with associated paramaters, @var{oe} and @var{oit},
## for the perceptron 1 vs All algorithm on an eegObject
## on the label @var{l} using cross-validation.
##
## @var{e} and @var{it} are respectively the smallest value for epsilon and
## numbers of iterations to test as parameters for the perceptron algorithm.
##
## Usage:
##
## @example
## [p, oe, oit] = optimizePerceptron(eeg, 1, 1e-4, [1000:1000:10000])
## @end example
## @end deftypefn

function [p, oe, oit] = optimizePerceptron(eeg, label, minEpsilon, nbIters)
  if(nargin < 4)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("optimizePerceptron: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("optimizePerceptron: second argument isn't a real")
  elseif(isreal(minEpsilon) == 0)
    error("optimizePerceptron: third argument isn't a real")
  elseif(isvector(nbIters) == 0)
    error("optimizePerceptron: fourth argument isn't a real")
  endif
  k = 1;
  epsilon = 0.1;
  while epsilon >= minEpsilon
    for i = 1 : length(nbIters)
      p = crossValidation(eeg, label, 10,
			  @learnPerceptron, epsilon, nbIters(i),
			  @predictPerceptron);
      m{k, 1} = mean(p);
      m{k, 2} = epsilon;
      m{k, 3} = nbIters(i);
      k++;
    endfor
    epsilon /= 10;
  endwhile
  [v, idx] = max(cell2mat(m)(:, 1));
  p = m{idx, 1};
  oe = m{idx, 2};
  oit = m{idx, 3};
endfunction
