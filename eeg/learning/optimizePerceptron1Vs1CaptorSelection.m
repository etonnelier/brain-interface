## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{p}, @var{oe}, @var{oit}] =} optimizePerceptron1Vs1CaptorSelection(@var{eeg}, @var{l}, @var{e}, @var{it}, @var{subset}, @var{nbCaptors})
## Return best performance with associated paramaters, @var{oe} and @var{oit},
## for the perceptron 1 vs 1 captor selection algorithm on an eegObject
## on the label @var{l} using cross-validation.
##
## @var{e} and @var{it} are respectively the smallest value for epsilon and
## numbers of iterations to test as parameters for the perceptron algorithm.
##
## algorithme learns the @var{nbCaptors} best captors for each 1Vs1 from the @var{subSet}
##
## Usage:
##
## @example
## [p, oe, oit] = optimizePerceptron1Vs1CaptorSelection(eeg, 1, 1e-4, [1000:1000:10000], subset, 50)
## @end example
## @end deftypefn

function [p, oe, oit] = optimizePerceptron1Vs1CaptorSelection(eeg, label, minEpsilon, nbIters, subset, nbCaptors)
  if(nargin < 4)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("optimizePerceptron1Vs1CaptorSelection: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("optimizePerceptron1Vs1CaptorSelection: second argument isn't a real")
  elseif(isreal(minEpsilon) == 0)
    error("optimizePerceptron1Vs1CaptorSelection: third argument isn't a real")
  elseif(isvector(nbIters) == 0)
    error("optimizePerceptron1Vs1CaptorSelection: fourth argument isn't a real")
  endif
  k = 1;
  epsilon = 0.1;
  while epsilon >= minEpsilon
    for i = 1 : length(nbIters)
      p = crossValidation(eeg, label, 10,
			  @learnPerceptron1Vs1CaptorsSelection, epsilon, nbIters(i), subset, nbCaptors,
			  @predictPerceptron1Vs1CaptorsSelection);
      m{k, 1} = mean(p);
      m{k, 2} = epsilon;
      m{k, 3} = nbIters(i);
      k++;
    endfor
    epsilon /= 10;
  endwhile
  [v, idx] = max(cell2mat(m)(:, 1));
  p = m{idx, 1};
  oe = m{idx, 2};
  oit = m{idx, 3};
endfunction
