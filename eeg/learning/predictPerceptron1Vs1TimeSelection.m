## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{yHat}, @var{s}] =} predictPerceptron1Vs1TimeSelection(@var{eeg}, @var{model})
## Return predicted labels @var{yHat} of an eegObject on the label type @var{label}
## with a '1 vs 1' and time selection perceptron learning model @var{model}.
##
## @var{s} is the succes of prediction between 0 and 1.
##
## Usage :
##
## @example
## [yHat, s] = predictPerceptron1Vs1TimeSelection(eeg, model);
## @end example
## @end deftypefn

function [yHat, s] = predictPerceptron1Vs1TimeSelection(eeg, model)
  if(nargin != 2)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("predictPerceptron1Vs1TimeSelection: first argument isn't an eegObject");
  elseif(strcmp(class(model), "modelObject") == 0)
    error("predictPerceptron1Vs1TimeSelection: second argument isn't a modelObject");
  elseif(istype(model, "perceptron1VS1TimeSelection") == 0)
    error("predictPerceptron1Vs1TimeSelection: second argument isn't a valid modelObject");
  endif
  X = eeg.base;
  Y = eeg.labels;
  myModel = model.model;
  w = myModel.model;
  combinaisons = myModel.combinaisons;
  timeNumbers = myModel.timesNumber;
  nbTemps = myModel.nbTimes;
  label = model.labels;
  nbExperiences = size(X, 1);
  nbLabelsTypes = size(Y, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("predictPerceptron1Vs1TimeSelection: label has to be in [1..%d], given %d",
    nbLabelsTypes,
    label);
  elseif(size(w, 1) != nbTemps*eeg.nbSensors)
    error("predictPerceptron1Vs1TimeSelection: model and eegObject aren't compatible");
  endif
  Y = Y(:, label);
%  disp('predictPerceptron1Vs1CaptorsSelection: Creating results matrix ...');
%  fflush(stdout);
  results = [];
  for i = 1 : size(combinaisons, 1)
    results = [results,...
       (getTimes(eeg, timeNumbers{combinaisons(i,1),combinaisons(i,2)}([1:nbTemps])).base ...
      * w(:,i))];
  end
%  disp('predictPerceptron1Vs1CaptorsSelection: done.');
%  fflush(stdout);
  classes = zeros(size(results, 1), length(unique(combinaisons)));
%  disp('predictPerceptron1Vs1CaptorsSelection: Results processing ...');
%  fflush(stdout);
  anc = 0;
  for i = 1 : size(results, 1)
     for j = 1 : size(results, 2)
      if results(i, j) > 0
        winners(i, j) = combinaisons(j, 1);
      else
        winners(i, j) = combinaisons(j, 2);      
      endif
      classes(i, winners(i, j))++;
     endfor
  endfor
  [maxValue, yHat] = max(classes, [], 2);
%  disp('predictPerceptron1Vs1CaptorsSelection: done');
%  fflush(stdout);
  classes = unique(Y);
%  disp('predictPerceptron1Vs1CaptorsSelection: Linking ...');
%  fflush(stdout);
  for i = 1 : length(classes)
    r(i, :) = (yHat == i);
  endfor
  for i = 1 : length(classes)
    yHat(r(i, :)) = classes(i, 1);
  endfor
  s = mean(Y == yHat);
%  disp(sprintf("predictPerceptron1Vs1CaptorsSelection: done\nmean success = %f%%", s*100));
endfunction
