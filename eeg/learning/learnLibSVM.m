## -*- texinfo -*- ##
## @deftypefn {Function File} {[@var{model}] =} learnLibSVM(@var{eeg}, @var{label}, @var{lerningParameters})
## Return a matrix @var{model} which is a libSVM learning model.
## Model is create with lerningParameters (see : http://www.csie.ntu.edu.tw/~cjlin/libsvm/).
## for an eegObject @var{eeg} on the label @var{label}.
## 
## Note :
## LibSVM mathlab must be installed on the system to use libSVM features.
## See http://www.csie.ntu.edu.tw/~cjlin/libsvm/ for more informations about libsvm.
## Methods must be accessible in the classpath.
## you can use addpath or rmpath.
##
## Usage :
##
## @example
## model = learnLibSVM(eeg, w, "-s 1 -q -t 0");
## @end example
## @end deftypefn

function model = learnLibSVM(eeg, label, lerningParameters)
  if(nargin != 3)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("learnLibSVM: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("learnLibSVM: second argument isn't a real");
  endif
  xapp = eeg.base;
  yapp = eeg.labels;
  nbLabelsTypes = size(yapp, 2);
  if(label < 1 || label > nbLabelsTypes)
    error("learnLibSVM: label has to be in [1..%d], given %d",
	  nbLabelsTypes,
	  label);
  endif
  w = svmtrain(yapp, xapp, lerningParameters);
  model = modelObject("libSVM", label, eeg.nbTimes, eeg.nbSensors, w);
endfunction
