## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{p} =} crossValidation(@var{eeg}, @var{l}, @var{k}, @dots{})
## Return a vector @var{p} of cross-validation performances
## with learn and test functions on an eegObject on the label @var{l}.
##
## @var{k} is the number of division, @var{k}=10 is commonly used.
##
## Usage:
##
## @example
## p = crossValidation(eeg, 1, 10, @@learnPerceptron, 0.0001, 5000, @@predictPerceptron)
## p = crossValidation(eeg, 2, 10, @@learnLibSVM, "-s 1 -q -t 0", @@predictLibSVM);
## @end example
## @end deftypefn

function p = crossValidation(eeg, label, k, varargin)
  if(nargin < 5)
    print_usage();
  elseif(strcmp(class(eeg), "eegObject") == 0)
    error("crossvalidation: first argument isn't an eegObject");
  elseif(isreal(label) == 0)
    error("crossvalidation: second argument isn't a real")
  elseif(isreal(k) == 0 || k < 1)
    error("crossvalidation: third argument isn't an integer")
  end
  h = getHandles(varargin{:});
  if(size(h, 2) < 2)
    error("crossvalidation: expected two function handles")
  end
  eeg = shuffle(eeg);
  nb = round(eeg.nbExperiences / round(k));
  p = [];
  for i = 1 : k
    begin = (i - 1) * nb + 1;
    lim = (i - 1) * nb + nb;
    if(i == k)
      lim = eeg.nbExperiences;
    end
    interval = [begin : lim];
    [eegTest, eegTrain] = split(eeg, interval);
    model = h{1, 1}(eegTrain, label, varargin{h{2, 1} : h{3, 1}});
    [yHat, s] = h{1, 2}(eegTest, model, varargin{h{2, 2}: h{3, 2}});
    p = [p; s];
  end
end
