## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{bool} =} get(@var{m}, @var{type})
## Return 1 if @var{m} is of type @var{type}, 0 else
## @end deftypefn

function bool = istype(model, type)
    if(nargin != 2)
        print_usage();
    end
    if(strcmp(class(model), "modelObject") == 0)
        error("modelObject: first argument isn't a modelObject");
    elseif(ischar(type) == 0)
        error("modelObject: property isn't a string");
    end
    bool = strcmp(model.id, type);
end
