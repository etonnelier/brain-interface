## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{v} =} subsref(@var{m}, @var{s})
## Index the contents of an object modelObject.
## @end deftypefn

function v = subsref(model, s)
  if(isempty(s))
    error("modelObject: missing index");
  end
  switch(s(1).type)
    case "."
      v = get(model, s(1).subs);
    otherwise
      error("modelObject: invalid subscript type");
  end
  if(numel(s) > 1)
    v = subsref(v, s(2:end));
  end
end
