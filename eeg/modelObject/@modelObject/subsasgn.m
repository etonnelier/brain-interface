## -*- texinfo -*- ##
## @deftypefn {Function File} {@var{m} =} subsasgn(@var{m}, @var{s}, @var{v})
## As an object modelObject is immutable this function is empty.
## @end deftypefn

function model = subsasgn(model, s, v)
end
