## -*- texinfo -*- ##
## @deftypefn {Function File} display(@var{m})
## Display the contents of an object modelObject.
## @end deftypefn

function display(model)
  fprintf("%s =\n", inputname(1));
  fprintf("\tid = \"%s\"\n", model.id);
  fprintf("\tlabels = [");
  fprintf("%d, ", model.labels);
  fprintf("\b\b]\n");
  fprintf("\ttypeinfo(model) = %s\n", typeinfo(model.model));
end
