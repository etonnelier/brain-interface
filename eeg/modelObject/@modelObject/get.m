## -*- texinfo -*- ##
## @deftypefn {Function File} {[...] =} get(@var{m}, @dots{})
## Return properties from an object modelObject.
##
## Usage:
##
## @example
## [id, m] = get(model, "id", "model");
## @end example
## @end deftypefn

function varargout = get(model, varargin)
  for i = 1 : numel(varargin)
    property = varargin{i};
    if(ischar(property) == 0)
      error("modelObject: property isn't a string");
    end
    istype = sprintf("is%s", model.id);
    switch(property)
      case "id"
        varargout{i} = model.id;
      case "labels"
        varargout{i} = model.labels;
      case "model"
        varargout{i} = model.model;
      case istype
        varargout{i} = true;
      otherwise
        error("modelObject: invalid property \"%s\"", property);
    end
  end
end
