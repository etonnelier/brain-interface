## -*- texinfo -*- ##
## @deftypefn {Function File} model(@var{id}, @var{l}, @var{nbTimes}, @var{nbSensors}, @var{m})
## Create a model object with
## @var{id} as the string identification of the model object,
## @car{l} as learned labels vector,
## @var{nbTimes} as number of time steps of eegObject,
## @var{nbSensors} as number of sensors of eegObject and
## @var{m} the model itself.
## @end deftypefn

function model = modelObject(id, l, nbTimes, nbSensors, m)
  if(nargin != 5)
    print_usage();
  elseif(!ischar(id))
    error("modelObject: first argument isn't a string");
  elseif(!isvector(l))
    error("modelObject: second argument isn't a vector");
 elseif(!isreal(nbTimes))
    error("modelObject: third argument isn't a real");
  elseif(!isreal(nbSensors))
    error("modelObject: fourth argument isn't a real");
  end
  model.id = id;
  model.labels = l;
  model.model = m;
  model.nbTimes = nbTimes;
  model.nbSensors = nbSensors;
  model = class(model, "modelObject");
end
