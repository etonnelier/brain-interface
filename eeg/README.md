# eeg package

*************

## build

Use the command below :

    $ sh build.sh

## install eeg package

From the Octave prompt :

    >> pkg prefix <install path>
    >> pkg install -local eeg.tar.gz

## use egg package

From the Octave prompt :

    >> pkg load eeg
