#!/bin/sh

make_index(){
    echo "$DIST_DIR >> EEG processing" > INDEX
    for dir in *
    do
        if [ -d $dir ] && [ "private" != $dir ]
        then
            INDEX_LIST="$dir $INDEX_LIST"
            echo `basename $dir` >> INDEX
            for source in `ls $dir`
            do
                sourcename=`basename $source`
                sourcename=`echo $sourcename | sed -r 's/@//g'`
                sourcename=`echo $sourcename | sed -r 's/\.m//g'`
                echo "  $sourcename" >> INDEX
            done
        fi
    done
}

make_description(){
    today=`date +"%Y-%m-%d"`
    sed -ri "s/^Date\:.*$/Date\: $today/g" DESCRIPTION
}

make_dist(){
    make_index
    make_description
    mkdir -p $DIST_DIR/inst
    cp INDEX $DIST_DIR/.
    cp DESCRIPTION $DIST_DIR/.
    cp COPYING $DIST_DIR/.
    for dir in $INDEX_LIST
    do
        cp -r $dir/* $DIST_DIR/inst/.
    done
    if [ -d private ]
    then
        cp -r private $DIST_DIR/inst/.
    fi
    tar -czvf $DIST_DIR.tar.gz $DIST_DIR
}

clean(){
    rm -rf $DIST_DIR
    rm -rf $DIST_DIR.tar.gz
    rm -rf INDEX
}

set -e
DIST_DIR=eeg

if [ -z $1 ]
then
    clean
    make_dist
else
    while [ "$1" != "" ]
    do
        if [ "$1" = "clean" ]
        then
            clean
        fi
        shift
    done
fi
