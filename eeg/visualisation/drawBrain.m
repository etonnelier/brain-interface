## -*- texinfo -*- ##
## @deftypefn {Function File} drawBrain(@var{data}, @var{nbTimes}, @var{coordsFile}, @var{out})
## Draw @var{nbTimes} images of brain according to the given positions file
## @var{coordsFile} of each sensor.
## 
## This function produce an image per time step in the given @var{out} directory.
## @end deftypefn

function drawBrain(data, nbTimes, coordsFile, out)
    if(nargin != 4)
        print_usage();
    end
    if(isvector(data) == 0)
        error("drawBrain: first argument isn't a vector");
    elseif(isreal(nbTimes) == 0)
        error("drawBrain: second argument isn't a real");
    elseif(ischar(coordsFile) == 0)
        error("drawBrain: third argument is't a string");
    elseif(ischar(out) == 0)
        error("drawBrain: fourth argument is't a string");
    end
    coords = load(coordsFile);
    coords = coords ./ 180.0 .* pi;
    nbSensors = size(data, 2) / nbTimes;
    if(mod(size(data, 2), nbTimes) != 0)
        error("drawBrain: wrong data format");
    elseif(size(coords, 1) != nbSensors || size(coords, 2) != 2)
        error("drawBrain: wrong coordinates format");
    end
    xcoords = cos(coords(:, 2)) .* coords(:, 1);
    ycoords = sin(coords(:, 2)) .* coords(:, 1);
    coords = [xcoords, ycoords];
    figures = [];
    if (size(data, 2) == 1)
        data = data';
    end
    for i = 1 : nbTimes
        f = figure;
        set(f, "visible", "off");
        hold on
        part = zeros(1, nbTimes);
        part(1, i) = 1;
        part = repmat(part, 1, nbSensors);
        xgrid = generateGrid(coords, 30);
        k = svmkernel(xgrid, "gaussian", 0.2, coords);
        ygrid = k * data(1, part == 1)';
        plotFrontiere(xgrid, ygrid, 1);
        shading("interp");
        view(2)
        grid off
        plot(xcoords, ycoords, "ro");
        axis("off");
        figures = [figures, f];
    end
    [status, msg] = mkdir(out);
    if(status != 1)
        error("drawBrain: %s", msg);
    end
    rootDir = pwd;
    cd(out)
    try
        frame = 0;
        for f = figures
            frame++;
            name = sprintf("%s_%03d", out, frame);
            print(f, name, "-dpng");
        end
    catch
        fprintf(stderr, "drawBrain: %s\n", lasterr());
        fprintf(stderr, "drawBrain: interrupt frame %3d / %3d\n", frame, length(figures));
    end
    cd(rootDir);
end
