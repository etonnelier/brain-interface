#!/usr/bin/env python


import os
import sys
import argparse
import logging
import re


def process(path, symbol, section):
    with open(path) as f:
        lines = f.readlines()
        logging.info('code extracted')
        doc = extract_doc(lines, symbol)
        logging.info('documentation extracted')
        doc = format_doc(doc)
        logging.info('documentation formatted')
        doc = check_latex_syntax(doc)
        logging.info('latex syntax checked')
        latex = to_latex(doc, section)
        logging.info('latex generated')
        print latex


def extract_doc(lines, symbol):
    doc = []
    flag = False
    for line in lines:
        if line.startswith(symbol):
            tmp = line.replace(symbol, '', 1).strip()
            tmp = re.sub(r'  +', ' ', tmp)
            doc.append(tmp)
            flag = True
        else:
            if flag:
                break
    return doc


def format_doc(doc):
    formatted = {
            'name' : '',
            'return' : [],
            'args' : [],
            'doc' : '',
            }
    if not re.search(r'\-\*\- ?texinfo ?\-\*\-', doc[0]):
        logging.error('texinfo not found')
    if not re.search(r'^@end deftypefn$', doc[-1]):
        logging.error('@end deftypenf not found')
    regex = r'^@deftypefn \{ ?Function File ?\} ?(?:\{(.*?)\= ?\})? ?(\w+) ?\((.*?)\)$'
    function = re.search(regex, doc[1])
    if function:
        gr1 = function.group(1)
        gr2 = function.group(2)
        gr3 = function.group(3)
        formatted['name'] = gr2
        if gr1:
            formatted['return'] = [x.strip() for x in gr1.split(',')]
        if gr3:
            formatted['args'] = [x.strip() for x in gr3.split(',')]
        for line in doc[2 : len(doc) - 1]:
            add = '\n\n'
            if line:
                add = line + ' '
            formatted['doc'] += add
        if not formatted['return'] or formatted['return'][0] == '':
            formatted['return'] = ['nothing']
        if not formatted['args'] or formatted['args'][0] == '':
            formatted['args'] = ['nothing']
    else:
        logging.error('wrong function header')
    return formatted


def check_latex_syntax(doc):
    regex = [
            (r'@var\{(.*?)\}', r'\\textbf{\\textsc{\g<1>}}'),
            (r'@dots\{\}', r'\\dots'),
            (r'@@', r'@'),
            (r'@example', r'\\begin{spverbatim}'),
            (r'@end example', r'\\end{spverbatim}'),
            ]
    for r in regex:
        doc['return'] = [re.sub(r[0], r[1], x) for x in doc['return']]
        doc['args'] = [re.sub(r[0], r[1], x) for x in doc['args']]
        doc['doc'] = re.sub(r[0], r[1], doc['doc'])
    return doc


def to_latex(doc, section):
    latex = ''
    latex += '\\' + section + '{' + doc['name'] + '}\n\n'
    latex += '\\begin{description}\n'
    for key, title in zip(['return', 'args'], ['return', 'parameters']):
        latex += '\\item[' + title + ' :] '
        for i, v in enumerate(doc[key]):
            if i > 0:
                latex += ', '
            latex += v
        latex += '\n'
    latex += '\\item[documentation :]\\hfill\n\n'
    latex += doc['doc']
    latex += '\\end{description}\n'
    return latex


def get_args():
    parser = argparse.ArgumentParser(
            description = 'Extract documentation from octave scripts.'
            )
    parser.add_argument(
            'files',
            nargs = '+',
            help = 'files to be processed'
            )
    parser.add_argument(
            '-c',
            '--comment',
            type = str,
            default = '##',
            help = 'comment character'
            )
    parser.add_argument(
            '-s',
            '--section',
            type = str,
            default = 'section',
            help = 'latex section header'
            )
    parser.add_argument(
            '-v',
            '--verbose',
            action = 'store_true',
            help = 'increase verbosity'
            )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    for path in map(os.path.abspath, args.files):
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        filename = os.path.basename(path) + ': '
        logging.basicConfig(
                level = level,
                format = '%(levelname)s: ' + filename + '%(message)s'
                )
        process(path, args.comment, args.section)
