#!/bin/sh

generate_doc(){
    echo "\\\chapter{Function reference}" > $DOC
    for dir in `ls -d -1 $DIST_DIR/*`
    do
        if [ -d $dir ] && [ `basename $dir` != "private" ]
        then
            section=`basename $dir`
            echo >> $DOC
            echo "\\section{$section}" >> $DOC
            echo >> $DOC
            for source in `ls -d -1 $dir/*`
            do
                if [ -d $source ]
                then
                    python doc.py -s "subsection" $source/* >> $DOC
                else
                    python doc.py -s "subsection" $source >> $DOC
                fi
            done
        fi
    done
}

set -e
DIST_DIR=./../eeg
DOC=./../doc/documentation/chapter5.tex

if [ -z $1 ]
then
    generate_doc
else
    while [ "$1" != "" ]
    do
        if [ "$1" = "clean" ]
        then
            clean
        fi
        shift
    done
fi
