#!/bin/bash

set -e

if [ -z $1 ]
then
    echo "error: need directory path"
    exit 1
fi

rootdir="$1"
cd $rootdir

cur=0
nb=`ls -l | wc -l`

for img in *.png
do
    cur=$(($cur+1))
    printf "crop frames: %3d%%\r" $(($cur*100/$nb))
    convert $img -trim -bordercolor White $img
done

echo
echo "convert to video:"

output=`basename $rootdir`
avconv -y -i "$output"_%03d.png -b:v 20000k -filter:v "setpts=1.0*PTS" "$output"_sm1.mp4
avconv -y -i "$output"_%03d.png -b:v 20000k -filter:v "setpts=3.0*PTS" "$output"_sm3.mp4
avconv -y -i "$output"_%03d.png -b:v 20000k -filter:v "setpts=5.0*PTS" "$output"_sm5.mp4

